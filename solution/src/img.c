#include "include/img.h"
#include <stdio.h>
#include <stdlib.h>

int rotate(struct image* img) {
    struct image temp;
    temp.height = img->width;
    temp.width = img->height;
    temp.data = (struct pixel*)malloc(sizeof(struct pixel) * img->width * img->height);
    if (temp.data == NULL) {
        fprintf(stderr, "There is not enough memory to complete the rotate operation\n");
        return 1;
    }
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            temp.data[j * img->height + (img->height - 1 - i)] = img->data[i * img->width + j];
        }
    }
    for (size_t i = 0; i < temp.height; i++) {
        for (size_t j = 0; j < temp.width; j++) {
            img->data[i * temp.width + j] = temp.data[i * temp.width + j];
        }
    }
    img->height = temp.height;
    img->width = temp.width;
    free(temp.data);
    return 0;
}
