#include "include/util.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        fprintf(stderr, "Expected 2 arguments %i given", argc);
        return 1;
    }
    struct image img = {.height = 0, .width = 0, .data = NULL};
    int error = reader(argv[1], &img);
    if (error != 0) {
        return error;
    }
    error = rotate(&img);
    if (error != 0) {
        return error;
    }
    error = writer(argv[2], &img);
    if (error != 0) {
        return error;
    }
    free(img.data);
    return 0;
}
