#include "include/bmp.h"
#include <stdlib.h>

static long get_padding(uint64_t width) {
    long temp = (long)4 - ((long)(sizeof(struct pixel) * width) % 4);
    return temp == 4 ? 0 : temp;
}

static enum write_status write_header(FILE* out, const struct image* img) {
    struct bmp_header new_header = {
        .bfType = 0x4d42,
        .bfReserved = 0,
        .bOffBits = 54,
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = img->height * (img->width * sizeof(struct pixel) + get_padding(img->width)),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
        .bfileSize = 0
    };

    new_header.bfileSize = new_header.biSize + new_header.biSizeImage;

    size_t x = fwrite(&new_header, sizeof(struct bmp_header), 1, out);
    if (x < 1) {
        fprintf(stderr, "An error ocurred while writing to output file");
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_data(FILE* out, const struct image* img) {
    long padding = get_padding(img->width);
    size_t x;
    struct pixel* buffer = img->data;
    for (size_t i = 0; i < img->height; i++) {
        x = fwrite(buffer, sizeof(struct pixel), img->width, out);
        if (x < img->width) {
            fprintf(stderr, "There was an error while writing image data\n");
            return WRITE_ERROR;
        }
        x = fseek(out, padding, SEEK_CUR);
        if (x != 0) {
            fprintf(stderr, "There was an error while writing image data\n");
            return WRITE_ERROR;
        }
        buffer += img->width;
    }
    return WRITE_OK;
}

static enum read_status read_bmp_header(FILE *in, struct bmp_header* header) {
    size_t x = fread(header, sizeof(struct bmp_header), 1, in);
    if (x < 1) {
        fprintf(stderr, "Given file is not a bmp image or its header is corrupted");
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_image_data(FILE* in, struct image* img) {
    img->data = (struct pixel*)malloc(sizeof(struct pixel) * img->height * img->width);
    if (img->data == NULL) {
        fprintf(stderr, "There is not enough memory to complete the operation");
        return READ_NOT_ENOUGH_MEMORY;
    }
    struct pixel* buffer = img->data;
    size_t x;
    long padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        x = fread(buffer, sizeof(struct pixel), img->width, in);
        if (x < img->width) {
            fprintf(stderr, "Image data corrupted\n");
            return READ_INVALID_BITS;
        }
        x = fseek(in, padding, SEEK_CUR);
        if (x != 0) {
            fprintf(stderr, "Image data corrupted\n");
            return READ_INVALID_BITS;
        }
        buffer += img->width;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header image_header;
    enum read_status error = read_bmp_header(in, &image_header);
    if (error != READ_OK) {
        fprintf(stderr, "Something wrong with image data\n");
        return READ_INVALID_BITS;
    }
    size_t x = fseek(in, image_header.bOffBits, SEEK_SET);
    if (x != 0) {
        fprintf(stderr, "Something went wrong when reading image header\n");
        return READ_INVALID_HEADER;
    }
    img->height = image_header.biHeight;
    img->width = image_header.biWidth;
    error = read_image_data(in, img);
    if (error != READ_OK) {
        return error;
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    enum write_status error = write_header(out, img);
    if (error != WRITE_OK) {
        return error;
    }
    error = write_data(out, img);
    if (error != WRITE_OK) {
        return error;
    }
    return WRITE_OK;
}

