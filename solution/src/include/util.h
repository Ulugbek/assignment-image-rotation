#ifndef UTILS
#define UTILS
#include "img.h"
#include <stdio.h>

int reader(const char* inputFileName, struct image* img);

int writer(const char* outputFileName, const struct image* img);

#endif
