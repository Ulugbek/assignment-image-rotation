#include "include/bmp.h"
#include "include/util.h"

int reader(const char* inputFileName, struct image* img) {
    FILE *in = NULL;
    in = fopen(inputFileName, "rb");
    if (in == NULL) {
        fprintf(stderr, "Input file not found\n");
        return 1;
    }

    enum read_status error = from_bmp(in, img);

    if (error != READ_OK) {
        fclose(in);
        return 1;
    }
    fclose(in);
    return 0;
}

int writer(const char* outputFileName, const struct image* img) {
    FILE *out = fopen(outputFileName, "wb");
    enum write_status error = to_bmp(out, img);
    if (error != WRITE_OK) {
        fclose(out);
        return 1;
    }
    fclose(out);
    return 0;
}
